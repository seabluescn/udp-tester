﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Communication.UDPClient
{
    public class UdpStateEventArgs : EventArgs
    {  
        public IPEndPoint remoteEndPoint;      
        public byte[] buffer = null;
    }
}
