﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPTest.Library.Model
{
    [Serializable]
    public class SendData
    {
        public string Name { get; set; } = string.Empty;

        public string Data { get; set; } = string.Empty;

        public DataType DataType { get; set; }
    }


}
