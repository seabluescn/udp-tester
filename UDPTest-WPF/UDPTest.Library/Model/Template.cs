﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPTest.Library.Model
{
    [Serializable]
    public class Template
    {
        public string LocalIP { get; set; }
        public int LocalPort { get; set; }
        public string RemoteIP { get; set; }
        public int RemotePort { get; set; }

        public List<SendData> SendDatas { get; set; }
    }
}
