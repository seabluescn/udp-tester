﻿using Stylet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UDPTest.Library.Utils;

namespace UDPTest.Pages
{
    public class ToolsViewModel : Screen
    {
        #region 字符串转十六进制

        public string txtStr2Hex_In { get; set; }
        public string txtStr2Hex_Out { get; set; }

        public void btnStr2Hex_Click()
        {
            string str = txtStr2Hex_In.Trim();

            if (!string.IsNullOrEmpty(str))
            {
                try
                {
                    var bytes = Encoding.UTF8.GetBytes(str);
                    txtStr2Hex_Out = ConvertHelper.ByteArrayToHexString(bytes);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public string txtHex2Str_In { get; set; }
        public string txtHex2Str_Out { get; set; }

        public void btnHex2Str_Click()
        {
            string hexstr = txtHex2Str_In.Trim();

            if (!string.IsNullOrEmpty(hexstr))
            {
                try
                {
                    var bytes = ConvertHelper.HexStringToByteArray(hexstr);
                    txtHex2Str_Out = Encoding.UTF8.GetString(bytes);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        #endregion

        #region 进制转换

        #region 整数

        public string txtDecimal { get; set; } = string.Empty;
        public string txtHexadecimal { get; set; } = string.Empty;
        public string txtBinary { get; set; } = string.Empty;

        public string lblStatusText { get; set; }
        public bool lblStatusVisible { get; set; }
        public Visibility lblStatusVisibility => lblStatusVisible ? Visibility.Visible : Visibility.Hidden;

        public void txtDecimal_TextChanged()
        {
            var DecStr = txtDecimal.Trim();
            if (!string.IsNullOrEmpty(DecStr))
            {
                if (int.TryParse(DecStr, out int result))
                {
                    txtHexadecimal = Convert.ToString(result, 16);
                    txtBinary = Convert.ToString(result, 2);

                    lblStatusVisible = false;
                }
                else
                {
                    lblStatusText = "error";
                    lblStatusVisible = true;
                }
            }
        }

        public void txtHexadecimal_TextChanged()
        {
            var HexStr = txtHexadecimal.Trim();
            if (!string.IsNullOrEmpty(HexStr))
            {
                try
                {
                    int result = int.Parse(HexStr, System.Globalization.NumberStyles.HexNumber);
                    txtDecimal = result.ToString();
                    txtBinary = Convert.ToString(result, 2);
                }
                catch
                {
                    lblStatusText = "error";
                    lblStatusVisible = true;
                }
            }
        }

        public void txtBinary_TextChanged()
        {
            var BinStr = txtBinary.Trim();
            if (!string.IsNullOrEmpty(BinStr))
            {
                try
                {
                    int result = Convert.ToInt32(BinStr, 2);
                    txtDecimal = result.ToString();
                    txtHexadecimal = Convert.ToString(result, 16);

                }
                catch
                {
                    lblStatusText = "error";
                    lblStatusVisible = true;
                }
            }
        }

        #endregion

        #region 浮点数

        public string txtFloat { get; set; } = string.Empty;
        public string txtFloatHex { get; set; } = string.Empty;
        public string txtFloatDecimal { get; set; } = string.Empty;

        public string lblFloatStatusText { get; set; }
        public bool lblFloatStatusVisible { get; set; }


        //浮点数转整数
        public void txtFloat_TextChanged()
        {
            try
            {
                string floatstr = txtFloat.Trim();
                float floatVal = float.Parse(floatstr, System.Globalization.NumberStyles.Float);
                byte[] bytes = BitConverter.GetBytes(floatVal);
                var HexStr = BitConverter.ToInt32(bytes, 0).ToString("X8");
                txtFloatHex = HexStr;

                int result = int.Parse(HexStr, System.Globalization.NumberStyles.HexNumber);
                txtFloatDecimal = result.ToString();

                lblFloatStatusVisible = false;
            }
            catch (Exception ex)
            {
                txtFloatHex = "";
                txtFloatDecimal = "";
                lblFloatStatusText = $"error:{ex.Message}";
                lblFloatStatusVisible = true;
            }
        }


        //整数转浮点数
        public void txtFloatDecimal_TextChanged()
        {
            try
            {
                string decimalstr = txtFloatDecimal.Trim();
                int decimalVal = Convert.ToInt32(decimalstr);
                byte[] bytes = BitConverter.GetBytes(decimalVal);

                var HexStrHex = BitConverter.ToInt32(bytes, 0).ToString("X8");
                txtFloatHex = HexStrHex;

                float floatVal = BitConverter.ToSingle(bytes, 0);
                txtFloat = floatVal.ToString();

                lblFloatStatusVisible = false;
            }
            catch (Exception ex)
            {
                txtFloatHex = "";
                txtFloat = "";
                lblFloatStatusText = $"error:{ex.Message}";
                lblFloatStatusVisible = true;
            }
        }

        #endregion

        #endregion

        #region CRC

        public string txtCRC_input { get; set; } = string.Empty;
        public string txtCRC_output { get; set; } = string.Empty;

        public void btnMakeCRC_Click()
        {
            try
            {
                string hexstr = txtCRC_input.Trim();
                hexstr += " 00 00";
                if (hexstr.Length > 1)
                {
                    var CRC = ConvertHelper.HexStringToByteArray(hexstr);
                    CRC = ConvertHelper.CalCRC16a(CRC);
                    var CRCstr = ConvertHelper.ByteArrayToHexString(CRC);
                    txtCRC_output = CRCstr;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion
    }
}
