﻿using Stylet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UDPTest.Library.Model;
using UDPTest.Library.Utils;

namespace UDPTest.Pages
{
    public class AddSendDataViewModel : Screen
    {
        public SendData SendData { get; set; } = new SendData();

        public bool IsSendHex { get; set; } = true;
        public bool IsSendText { get; set; }

        public void Save()
        {
            SendData.DataType = IsSendHex ? DataType.Hex : DataType.Text;

            SendData.Name = SendData.Name.Trim();
            SendData.Data = SendData.Data.Trim();

            if (SendData.Name.Length == 0)
            {
                MessageBox.Show("请输入标识", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (SendData.Data.Length == 0)
            {
                MessageBox.Show("请输入数据", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (IsSendHex)
            {
                try
                {
                     ConvertHelper.HexStringToByteArray(SendData.Data);
                }
                catch
                {
                    MessageBox.Show("无效的数据", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

            this.RequestClose(true);
        }

        public void Close()
        {
            this.RequestClose(false);
        }
    }
}
