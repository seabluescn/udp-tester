﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPTest.Pages
{
    public interface IViewFactory
    {
        AboutViewModel AboutViewModel();
        AddSendDataViewModel AddSendDataViewModel();
        ToolsViewModel ToolsViewModel();
    }
}
