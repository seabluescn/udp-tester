﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UDPTest.Pages
{
    /// <summary>
    /// MainView.xaml 的交互逻辑
    /// </summary>
    public partial class MainView : MetroWindow
    {
        public MainView()
        {
            InitializeComponent();

            this.Loaded += MainView_Loaded;
            this.Closed += MainView_Closed;
        }

        private void MainView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Width = Properties.Settings.Default.WinWidth;
            this.Height = Properties.Settings.Default.WinHeight;
        }

        private void MainView_Closed(object sender, EventArgs e)
        {
            Properties.Settings.Default.WinWidth = this.Width;
            Properties.Settings.Default.WinHeight = this.Height;
            Properties.Settings.Default.Save();
        }

        private void txtReceivedTextList_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.txtReceivedTextList.ScrollToEnd();
        }

        private void txtSendTextList_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.txtSendTextList.ScrollToEnd();
        }
    }
}
