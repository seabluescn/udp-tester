﻿using Stylet.Logging;
using Stylet;
using StyletIoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDPTest.Pages;

namespace UDPTest
{
    internal class Bootstrapper : Bootstrapper<MainViewModel>
    {
        protected override void ConfigureIoC(IStyletIoCBuilder builder)
        {
            builder.Bind<IViewFactory>().ToAbstractFactory();
        }
    }
}
