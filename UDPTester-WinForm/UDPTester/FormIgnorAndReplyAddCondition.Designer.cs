﻿namespace UDPTester
{
    partial class FormIgnorAndReplyAddCondition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radJoinOr = new System.Windows.Forms.RadioButton();
            this.radJoinAnd = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radRegular = new System.Windows.Forms.RadioButton();
            this.radContain = new System.Windows.Forms.RadioButton();
            this.radEndWith = new System.Windows.Forms.RadioButton();
            this.radStartWith = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.radEqual = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtConditionText = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "联合类型：";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radJoinOr);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.radJoinAnd);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(468, 100);
            this.panel1.TabIndex = 1;
            // 
            // radJoinOr
            // 
            this.radJoinOr.AutoSize = true;
            this.radJoinOr.Location = new System.Drawing.Point(141, 59);
            this.radJoinOr.Name = "radJoinOr";
            this.radJoinOr.Size = new System.Drawing.Size(187, 19);
            this.radJoinOr.TabIndex = 1;
            this.radJoinOr.Text = "OR （一个满足即满足）";
            this.radJoinOr.UseVisualStyleBackColor = true;
            // 
            // radJoinAnd
            // 
            this.radJoinAnd.AutoSize = true;
            this.radJoinAnd.Checked = true;
            this.radJoinAnd.Location = new System.Drawing.Point(141, 25);
            this.radJoinAnd.Name = "radJoinAnd";
            this.radJoinAnd.Size = new System.Drawing.Size(173, 19);
            this.radJoinAnd.TabIndex = 0;
            this.radJoinAnd.TabStop = true;
            this.radJoinAnd.Text = "AND(全部满足才满足)";
            this.radJoinAnd.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radRegular);
            this.panel2.Controls.Add(this.radContain);
            this.panel2.Controls.Add(this.radEndWith);
            this.panel2.Controls.Add(this.radStartWith);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.radEqual);
            this.panel2.Location = new System.Drawing.Point(12, 135);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(468, 158);
            this.panel2.TabIndex = 2;
            // 
            // radRegular
            // 
            this.radRegular.AutoSize = true;
            this.radRegular.Location = new System.Drawing.Point(141, 112);
            this.radRegular.Name = "radRegular";
            this.radRegular.Size = new System.Drawing.Size(133, 19);
            this.radRegular.TabIndex = 4;
            this.radRegular.Text = "满足正则表达式";
            this.radRegular.UseVisualStyleBackColor = true;
            // 
            // radContain
            // 
            this.radContain.AutoSize = true;
            this.radContain.Location = new System.Drawing.Point(297, 72);
            this.radContain.Name = "radContain";
            this.radContain.Size = new System.Drawing.Size(58, 19);
            this.radContain.TabIndex = 3;
            this.radContain.Text = "包含";
            this.radContain.UseVisualStyleBackColor = true;
            // 
            // radEndWith
            // 
            this.radEndWith.AutoSize = true;
            this.radEndWith.Location = new System.Drawing.Point(141, 70);
            this.radEndWith.Name = "radEndWith";
            this.radEndWith.Size = new System.Drawing.Size(73, 19);
            this.radEndWith.TabIndex = 2;
            this.radEndWith.Text = "结束于";
            this.radEndWith.UseVisualStyleBackColor = true;
            // 
            // radStartWith
            // 
            this.radStartWith.AutoSize = true;
            this.radStartWith.Location = new System.Drawing.Point(297, 27);
            this.radStartWith.Name = "radStartWith";
            this.radStartWith.Size = new System.Drawing.Size(73, 19);
            this.radStartWith.TabIndex = 1;
            this.radStartWith.Text = "开始于";
            this.radStartWith.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "条件类型：";
            // 
            // radEqual
            // 
            this.radEqual.AutoSize = true;
            this.radEqual.Checked = true;
            this.radEqual.Location = new System.Drawing.Point(141, 25);
            this.radEqual.Name = "radEqual";
            this.radEqual.Size = new System.Drawing.Size(88, 19);
            this.radEqual.TabIndex = 0;
            this.radEqual.TabStop = true;
            this.radEqual.Text = "完全等于";
            this.radEqual.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 324);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "条件文本：";
            // 
            // txtConditionText
            // 
            this.txtConditionText.Location = new System.Drawing.Point(153, 324);
            this.txtConditionText.Name = "txtConditionText";
            this.txtConditionText.Size = new System.Drawing.Size(280, 25);
            this.txtConditionText.TabIndex = 4;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(153, 372);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 37);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "OK";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // FormIgnorAndReplyAddCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 430);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtConditionText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "FormIgnorAndReplyAddCondition";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "条件";
            this.Load += new System.EventHandler(this.FormIgnorAndReplyAddCondition_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radJoinOr;
        private System.Windows.Forms.RadioButton radJoinAnd;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radRegular;
        private System.Windows.Forms.RadioButton radContain;
        private System.Windows.Forms.RadioButton radEndWith;
        private System.Windows.Forms.RadioButton radStartWith;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radEqual;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtConditionText;
        private System.Windows.Forms.Button btnAdd;
    }
}