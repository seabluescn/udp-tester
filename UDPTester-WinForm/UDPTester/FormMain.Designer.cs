﻿namespace UDPTester
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAbout = new System.Windows.Forms.LinkLabel();
            this.btnTools = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.numRemotePort = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRemoteIP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.numLocalPort = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLocalIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxReceived = new System.Windows.Forms.GroupBox();
            this.radShowString = new System.Windows.Forms.RadioButton();
            this.radShowHex = new System.Windows.Forms.RadioButton();
            this.btnIgnorAndReply = new System.Windows.Forms.Button();
            this.btnReflashStop = new System.Windows.Forms.Button();
            this.btnReflashStart = new System.Windows.Forms.Button();
            this.checkNotShowDetail = new System.Windows.Forms.CheckBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.txtReceived = new System.Windows.Forms.TextBox();
            this.groupBoxSend = new System.Windows.Forms.GroupBox();
            this.btnClearSent = new System.Windows.Forms.Button();
            this.txtSent = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radAutoSendString = new System.Windows.Forms.RadioButton();
            this.radAutoSendHex = new System.Windows.Forms.RadioButton();
            this.radSendString = new System.Windows.Forms.RadioButton();
            this.radSendHex = new System.Windows.Forms.RadioButton();
            this.btnSelAutoTemplate = new System.Windows.Forms.Button();
            this.btnAutoSendStop = new System.Windows.Forms.Button();
            this.btnAutoSendStart = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.numAutoSendTicks = new System.Windows.Forms.NumericUpDown();
            this.btnSelStringTemplate = new System.Windows.Forms.Button();
            this.txtAutoSendHex = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSendString = new System.Windows.Forms.Button();
            this.txtSendString = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkAutoCRC = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRemotePort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLocalPort)).BeginInit();
            this.groupBoxReceived.SuspendLayout();
            this.groupBoxSend.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAutoSendTicks)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAbout);
            this.groupBox1.Controls.Add(this.btnTools);
            this.groupBox1.Controls.Add(this.btnDisconnect);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.numRemotePort);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtRemoteIP);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.numLocalPort);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtLocalIP);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1083, 105);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "连接";
            // 
            // btnAbout
            // 
            this.btnAbout.AutoSize = true;
            this.btnAbout.Location = new System.Drawing.Point(1014, 50);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(37, 15);
            this.btnAbout.TabIndex = 11;
            this.btnAbout.TabStop = true;
            this.btnAbout.Text = "关于";
            this.btnAbout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAbout_LinkClicked);
            // 
            // btnTools
            // 
            this.btnTools.Location = new System.Drawing.Point(875, 33);
            this.btnTools.Name = "btnTools";
            this.btnTools.Size = new System.Drawing.Size(106, 49);
            this.btnTools.TabIndex = 10;
            this.btnTools.Text = "工具";
            this.btnTools.UseVisualStyleBackColor = true;
            this.btnTools.Click += new System.EventHandler(this.btnTools_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(741, 33);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(106, 49);
            this.btnDisconnect.TabIndex = 9;
            this.btnDisconnect.Text = "断开";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(606, 33);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(106, 49);
            this.btnConnect.TabIndex = 8;
            this.btnConnect.Text = "连接";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // numRemotePort
            // 
            this.numRemotePort.Location = new System.Drawing.Point(482, 62);
            this.numRemotePort.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numRemotePort.Name = "numRemotePort";
            this.numRemotePort.Size = new System.Drawing.Size(80, 25);
            this.numRemotePort.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(373, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "目标机器端口:";
            // 
            // txtRemoteIP
            // 
            this.txtRemoteIP.Location = new System.Drawing.Point(154, 62);
            this.txtRemoteIP.Name = "txtRemoteIP";
            this.txtRemoteIP.Size = new System.Drawing.Size(159, 25);
            this.txtRemoteIP.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "目标机器IP：";
            // 
            // numLocalPort
            // 
            this.numLocalPort.Location = new System.Drawing.Point(482, 28);
            this.numLocalPort.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numLocalPort.Name = "numLocalPort";
            this.numLocalPort.Size = new System.Drawing.Size(80, 25);
            this.numLocalPort.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(373, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "本机端口:";
            // 
            // txtLocalIP
            // 
            this.txtLocalIP.Location = new System.Drawing.Point(154, 28);
            this.txtLocalIP.Name = "txtLocalIP";
            this.txtLocalIP.Size = new System.Drawing.Size(159, 25);
            this.txtLocalIP.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "本机IP：";
            // 
            // groupBoxReceived
            // 
            this.groupBoxReceived.Controls.Add(this.radShowString);
            this.groupBoxReceived.Controls.Add(this.radShowHex);
            this.groupBoxReceived.Controls.Add(this.btnIgnorAndReply);
            this.groupBoxReceived.Controls.Add(this.btnReflashStop);
            this.groupBoxReceived.Controls.Add(this.btnReflashStart);
            this.groupBoxReceived.Controls.Add(this.checkNotShowDetail);
            this.groupBoxReceived.Controls.Add(this.btnClear);
            this.groupBoxReceived.Controls.Add(this.btnCopy);
            this.groupBoxReceived.Controls.Add(this.txtReceived);
            this.groupBoxReceived.Location = new System.Drawing.Point(12, 123);
            this.groupBoxReceived.Name = "groupBoxReceived";
            this.groupBoxReceived.Size = new System.Drawing.Size(1083, 370);
            this.groupBoxReceived.TabIndex = 1;
            this.groupBoxReceived.TabStop = false;
            this.groupBoxReceived.Text = "接收";
            // 
            // radShowString
            // 
            this.radShowString.AutoSize = true;
            this.radShowString.Location = new System.Drawing.Point(75, 340);
            this.radShowString.Name = "radShowString";
            this.radShowString.Size = new System.Drawing.Size(76, 19);
            this.radShowString.TabIndex = 15;
            this.radShowString.Text = "String";
            this.radShowString.UseVisualStyleBackColor = true;
            this.radShowString.CheckedChanged += new System.EventHandler(this.radShowString_CheckedChanged);
            // 
            // radShowHex
            // 
            this.radShowHex.AutoSize = true;
            this.radShowHex.Checked = true;
            this.radShowHex.Location = new System.Drawing.Point(16, 340);
            this.radShowHex.Name = "radShowHex";
            this.radShowHex.Size = new System.Drawing.Size(52, 19);
            this.radShowHex.TabIndex = 14;
            this.radShowHex.TabStop = true;
            this.radShowHex.Text = "HEX";
            this.radShowHex.UseVisualStyleBackColor = true;
            this.radShowHex.CheckedChanged += new System.EventHandler(this.radShowHex_CheckedChanged);
            // 
            // btnIgnorAndReply
            // 
            this.btnIgnorAndReply.Location = new System.Drawing.Point(944, 297);
            this.btnIgnorAndReply.Name = "btnIgnorAndReply";
            this.btnIgnorAndReply.Size = new System.Drawing.Size(129, 35);
            this.btnIgnorAndReply.TabIndex = 13;
            this.btnIgnorAndReply.Text = "忽略与自动回复";
            this.btnIgnorAndReply.UseVisualStyleBackColor = true;
            this.btnIgnorAndReply.Click += new System.EventHandler(this.btnIgnorList_Click);
            // 
            // btnReflashStop
            // 
            this.btnReflashStop.Location = new System.Drawing.Point(944, 93);
            this.btnReflashStop.Name = "btnReflashStop";
            this.btnReflashStop.Size = new System.Drawing.Size(129, 35);
            this.btnReflashStop.TabIndex = 12;
            this.btnReflashStop.Text = "停止刷新";
            this.btnReflashStop.UseVisualStyleBackColor = true;
            this.btnReflashStop.Click += new System.EventHandler(this.btnReflashStop_Click);
            // 
            // btnReflashStart
            // 
            this.btnReflashStart.Enabled = false;
            this.btnReflashStart.Location = new System.Drawing.Point(944, 25);
            this.btnReflashStart.Name = "btnReflashStart";
            this.btnReflashStart.Size = new System.Drawing.Size(129, 35);
            this.btnReflashStart.TabIndex = 11;
            this.btnReflashStart.Text = "实时显示";
            this.btnReflashStart.UseVisualStyleBackColor = true;
            this.btnReflashStart.Click += new System.EventHandler(this.btnReflashStart_Click);
            // 
            // checkNotShowDetail
            // 
            this.checkNotShowDetail.AutoSize = true;
            this.checkNotShowDetail.Checked = true;
            this.checkNotShowDetail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkNotShowDetail.Location = new System.Drawing.Point(173, 341);
            this.checkNotShowDetail.Name = "checkNotShowDetail";
            this.checkNotShowDetail.Size = new System.Drawing.Size(104, 19);
            this.checkNotShowDetail.TabIndex = 9;
            this.checkNotShowDetail.Text = "略显长数据";
            this.checkNotShowDetail.UseVisualStyleBackColor = true;
            this.checkNotShowDetail.CheckedChanged += new System.EventHandler(this.checkNotShowDetail_CheckedChanged);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(944, 229);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(129, 35);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "清空";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(944, 161);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(129, 35);
            this.btnCopy.TabIndex = 7;
            this.btnCopy.Text = "拷贝到粘贴板";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // txtReceived
            // 
            this.txtReceived.Location = new System.Drawing.Point(16, 25);
            this.txtReceived.Multiline = true;
            this.txtReceived.Name = "txtReceived";
            this.txtReceived.ReadOnly = true;
            this.txtReceived.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtReceived.Size = new System.Drawing.Size(912, 307);
            this.txtReceived.TabIndex = 0;
            // 
            // groupBoxSend
            // 
            this.groupBoxSend.Controls.Add(this.chkAutoCRC);
            this.groupBoxSend.Controls.Add(this.btnClearSent);
            this.groupBoxSend.Controls.Add(this.txtSent);
            this.groupBoxSend.Controls.Add(this.panel1);
            this.groupBoxSend.Controls.Add(this.radSendString);
            this.groupBoxSend.Controls.Add(this.radSendHex);
            this.groupBoxSend.Controls.Add(this.btnSelAutoTemplate);
            this.groupBoxSend.Controls.Add(this.btnAutoSendStop);
            this.groupBoxSend.Controls.Add(this.btnAutoSendStart);
            this.groupBoxSend.Controls.Add(this.label9);
            this.groupBoxSend.Controls.Add(this.numAutoSendTicks);
            this.groupBoxSend.Controls.Add(this.btnSelStringTemplate);
            this.groupBoxSend.Controls.Add(this.txtAutoSendHex);
            this.groupBoxSend.Controls.Add(this.label8);
            this.groupBoxSend.Controls.Add(this.btnSendString);
            this.groupBoxSend.Controls.Add(this.txtSendString);
            this.groupBoxSend.Controls.Add(this.label5);
            this.groupBoxSend.Location = new System.Drawing.Point(12, 499);
            this.groupBoxSend.Name = "groupBoxSend";
            this.groupBoxSend.Size = new System.Drawing.Size(1083, 266);
            this.groupBoxSend.TabIndex = 1;
            this.groupBoxSend.TabStop = false;
            this.groupBoxSend.Text = "发送";
            // 
            // btnClearSent
            // 
            this.btnClearSent.Location = new System.Drawing.Point(944, 218);
            this.btnClearSent.Name = "btnClearSent";
            this.btnClearSent.Size = new System.Drawing.Size(129, 35);
            this.btnClearSent.TabIndex = 16;
            this.btnClearSent.Text = "清空";
            this.btnClearSent.UseVisualStyleBackColor = true;
            this.btnClearSent.Click += new System.EventHandler(this.btnClearSent_Click);
            // 
            // txtSent
            // 
            this.txtSent.Location = new System.Drawing.Point(16, 114);
            this.txtSent.Multiline = true;
            this.txtSent.Name = "txtSent";
            this.txtSent.ReadOnly = true;
            this.txtSent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSent.Size = new System.Drawing.Size(912, 142);
            this.txtSent.TabIndex = 16;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radAutoSendString);
            this.panel1.Controls.Add(this.radAutoSendHex);
            this.panel1.Location = new System.Drawing.Point(88, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(146, 35);
            this.panel1.TabIndex = 16;
            // 
            // radAutoSendString
            // 
            this.radAutoSendString.AutoSize = true;
            this.radAutoSendString.Location = new System.Drawing.Point(62, 8);
            this.radAutoSendString.Name = "radAutoSendString";
            this.radAutoSendString.Size = new System.Drawing.Size(76, 19);
            this.radAutoSendString.TabIndex = 21;
            this.radAutoSendString.Text = "String";
            this.radAutoSendString.UseVisualStyleBackColor = true;
            // 
            // radAutoSendHex
            // 
            this.radAutoSendHex.AutoSize = true;
            this.radAutoSendHex.Checked = true;
            this.radAutoSendHex.Location = new System.Drawing.Point(3, 8);
            this.radAutoSendHex.Name = "radAutoSendHex";
            this.radAutoSendHex.Size = new System.Drawing.Size(52, 19);
            this.radAutoSendHex.TabIndex = 20;
            this.radAutoSendHex.TabStop = true;
            this.radAutoSendHex.Text = "HEX";
            this.radAutoSendHex.UseVisualStyleBackColor = true;
            // 
            // radSendString
            // 
            this.radSendString.AutoSize = true;
            this.radSendString.Location = new System.Drawing.Point(150, 33);
            this.radSendString.Name = "radSendString";
            this.radSendString.Size = new System.Drawing.Size(76, 19);
            this.radSendString.TabIndex = 17;
            this.radSendString.Text = "String";
            this.radSendString.UseVisualStyleBackColor = true;
            // 
            // radSendHex
            // 
            this.radSendHex.AutoSize = true;
            this.radSendHex.Checked = true;
            this.radSendHex.Location = new System.Drawing.Point(91, 33);
            this.radSendHex.Name = "radSendHex";
            this.radSendHex.Size = new System.Drawing.Size(52, 19);
            this.radSendHex.TabIndex = 16;
            this.radSendHex.TabStop = true;
            this.radSendHex.Text = "HEX";
            this.radSendHex.UseVisualStyleBackColor = true;
            this.radSendHex.CheckedChanged += new System.EventHandler(this.radSendHex_CheckedChanged);
            // 
            // btnSelAutoTemplate
            // 
            this.btnSelAutoTemplate.Location = new System.Drawing.Point(770, 67);
            this.btnSelAutoTemplate.Name = "btnSelAutoTemplate";
            this.btnSelAutoTemplate.Size = new System.Drawing.Size(42, 25);
            this.btnSelAutoTemplate.TabIndex = 15;
            this.btnSelAutoTemplate.Text = "...";
            this.btnSelAutoTemplate.UseVisualStyleBackColor = true;
            this.btnSelAutoTemplate.Click += new System.EventHandler(this.btnSelAutoTemplate_Click);
            // 
            // btnAutoSendStop
            // 
            this.btnAutoSendStop.Enabled = false;
            this.btnAutoSendStop.Location = new System.Drawing.Point(1011, 66);
            this.btnAutoSendStop.Name = "btnAutoSendStop";
            this.btnAutoSendStop.Size = new System.Drawing.Size(64, 25);
            this.btnAutoSendStop.TabIndex = 14;
            this.btnAutoSendStop.Text = "停止";
            this.btnAutoSendStop.UseVisualStyleBackColor = true;
            this.btnAutoSendStop.Click += new System.EventHandler(this.btnAutoSendStop_Click);
            // 
            // btnAutoSendStart
            // 
            this.btnAutoSendStart.Location = new System.Drawing.Point(939, 66);
            this.btnAutoSendStart.Name = "btnAutoSendStart";
            this.btnAutoSendStart.Size = new System.Drawing.Size(66, 25);
            this.btnAutoSendStart.TabIndex = 13;
            this.btnAutoSendStart.Text = "启动";
            this.btnAutoSendStart.UseVisualStyleBackColor = true;
            this.btnAutoSendStart.Click += new System.EventHandler(this.btnAutoSendStart_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(905, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 15);
            this.label9.TabIndex = 12;
            this.label9.Text = "ms";
            // 
            // numAutoSendTicks
            // 
            this.numAutoSendTicks.Location = new System.Drawing.Point(818, 67);
            this.numAutoSendTicks.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numAutoSendTicks.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numAutoSendTicks.Name = "numAutoSendTicks";
            this.numAutoSendTicks.Size = new System.Drawing.Size(82, 25);
            this.numAutoSendTicks.TabIndex = 11;
            this.numAutoSendTicks.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numAutoSendTicks.ValueChanged += new System.EventHandler(this.numAutoSendTicks_ValueChanged);
            // 
            // btnSelStringTemplate
            // 
            this.btnSelStringTemplate.Location = new System.Drawing.Point(770, 33);
            this.btnSelStringTemplate.Name = "btnSelStringTemplate";
            this.btnSelStringTemplate.Size = new System.Drawing.Size(42, 25);
            this.btnSelStringTemplate.TabIndex = 9;
            this.btnSelStringTemplate.Text = "...";
            this.btnSelStringTemplate.UseVisualStyleBackColor = true;
            this.btnSelStringTemplate.Click += new System.EventHandler(this.btnSelStringTemplate_Click);
            // 
            // txtAutoSendHex
            // 
            this.txtAutoSendHex.Location = new System.Drawing.Point(240, 67);
            this.txtAutoSendHex.Name = "txtAutoSendHex";
            this.txtAutoSendHex.Size = new System.Drawing.Size(524, 25);
            this.txtAutoSendHex.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "循环发送：";
            // 
            // btnSendString
            // 
            this.btnSendString.Location = new System.Drawing.Point(939, 31);
            this.btnSendString.Name = "btnSendString";
            this.btnSendString.Size = new System.Drawing.Size(136, 25);
            this.btnSendString.TabIndex = 2;
            this.btnSendString.Text = "发送";
            this.btnSendString.UseVisualStyleBackColor = true;
            this.btnSendString.Click += new System.EventHandler(this.btnSendString_Click);
            // 
            // txtSendString
            // 
            this.txtSendString.Location = new System.Drawing.Point(240, 32);
            this.txtSendString.Name = "txtSendString";
            this.txtSendString.Size = new System.Drawing.Size(524, 25);
            this.txtSendString.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "发送：";
            // 
            // chkAutoCRC
            // 
            this.chkAutoCRC.AutoSize = true;
            this.chkAutoCRC.Location = new System.Drawing.Point(819, 38);
            this.chkAutoCRC.Name = "chkAutoCRC";
            this.chkAutoCRC.Size = new System.Drawing.Size(113, 19);
            this.chkAutoCRC.TabIndex = 18;
            this.chkAutoCRC.Text = "追加CRC数据";
            this.chkAutoCRC.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 769);
            this.Controls.Add(this.groupBoxSend);
            this.Controls.Add(this.groupBoxReceived);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UDP Tester";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRemotePort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLocalPort)).EndInit();
            this.groupBoxReceived.ResumeLayout(false);
            this.groupBoxReceived.PerformLayout();
            this.groupBoxSend.ResumeLayout(false);
            this.groupBoxSend.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAutoSendTicks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxReceived;
        private System.Windows.Forms.GroupBox groupBoxSend;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.NumericUpDown numRemotePort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRemoteIP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numLocalPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLocalIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.TextBox txtReceived;
        private System.Windows.Forms.Button btnSendString;
        private System.Windows.Forms.TextBox txtSendString;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkNotShowDetail;
        private System.Windows.Forms.Button btnAutoSendStop;
        private System.Windows.Forms.Button btnAutoSendStart;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numAutoSendTicks;
        private System.Windows.Forms.Button btnSelStringTemplate;
        private System.Windows.Forms.TextBox txtAutoSendHex;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnReflashStop;
        private System.Windows.Forms.Button btnReflashStart;
        private System.Windows.Forms.Button btnIgnorAndReply;
        private System.Windows.Forms.Button btnTools;
        private System.Windows.Forms.RadioButton radShowString;
        private System.Windows.Forms.RadioButton radShowHex;
        private System.Windows.Forms.Button btnSelAutoTemplate;
        private System.Windows.Forms.LinkLabel btnAbout;
        private System.Windows.Forms.RadioButton radSendString;
        private System.Windows.Forms.RadioButton radSendHex;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radAutoSendString;
        private System.Windows.Forms.RadioButton radAutoSendHex;
        private System.Windows.Forms.Button btnClearSent;
        private System.Windows.Forms.TextBox txtSent;
        private System.Windows.Forms.CheckBox chkAutoCRC;
    }
}

