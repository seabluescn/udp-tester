﻿namespace UDPTester
{
    partial class FormIgnorAndReplyAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridConditions = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkIgnor = new System.Windows.Forms.CheckBox();
            this.chkAutoReply = new System.Windows.Forms.CheckBox();
            this.radReplyString = new System.Windows.Forms.RadioButton();
            this.radReplyHex = new System.Windows.Forms.RadioButton();
            this.txtReplyContent = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnConditionAdd = new System.Windows.Forms.Button();
            this.btnConditionRemove = new System.Windows.Forms.Button();
            this.panelReply = new System.Windows.Forms.Panel();
            this.btnConditionDetail = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridConditions)).BeginInit();
            this.panelReply.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "标识：";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(154, 37);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(411, 25);
            this.txtTitle.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "条件列表：";
            // 
            // dataGridConditions
            // 
            this.dataGridConditions.AllowUserToAddRows = false;
            this.dataGridConditions.AllowUserToDeleteRows = false;
            this.dataGridConditions.AllowUserToOrderColumns = true;
            this.dataGridConditions.AllowUserToResizeRows = false;
            this.dataGridConditions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridConditions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridConditions.Location = new System.Drawing.Point(154, 93);
            this.dataGridConditions.MultiSelect = false;
            this.dataGridConditions.Name = "dataGridConditions";
            this.dataGridConditions.ReadOnly = true;
            this.dataGridConditions.RowHeadersWidth = 51;
            this.dataGridConditions.RowTemplate.Height = 27;
            this.dataGridConditions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridConditions.Size = new System.Drawing.Size(411, 213);
            this.dataGridConditions.TabIndex = 3;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "JoinType";
            this.Column1.HeaderText = "联合类型";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "ConditionType";
            this.Column2.HeaderText = "条件类型";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 125;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Content";
            this.Column3.HeaderText = "条件内容";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 160;
            // 
            // chkIgnor
            // 
            this.chkIgnor.AutoSize = true;
            this.chkIgnor.Location = new System.Drawing.Point(154, 341);
            this.chkIgnor.Name = "chkIgnor";
            this.chkIgnor.Size = new System.Drawing.Size(172, 19);
            this.chkIgnor.TabIndex = 4;
            this.chkIgnor.Text = "忽略显示（只显示.）";
            this.chkIgnor.UseVisualStyleBackColor = true;
            // 
            // chkAutoReply
            // 
            this.chkAutoReply.AutoSize = true;
            this.chkAutoReply.Location = new System.Drawing.Point(154, 381);
            this.chkAutoReply.Name = "chkAutoReply";
            this.chkAutoReply.Size = new System.Drawing.Size(89, 19);
            this.chkAutoReply.TabIndex = 5;
            this.chkAutoReply.Text = "自动回复";
            this.chkAutoReply.UseVisualStyleBackColor = true;
            this.chkAutoReply.CheckedChanged += new System.EventHandler(this.chkAutoReply_CheckedChanged);
            // 
            // radReplyString
            // 
            this.radReplyString.AutoSize = true;
            this.radReplyString.Checked = true;
            this.radReplyString.Location = new System.Drawing.Point(133, 19);
            this.radReplyString.Name = "radReplyString";
            this.radReplyString.Size = new System.Drawing.Size(73, 19);
            this.radReplyString.TabIndex = 6;
            this.radReplyString.TabStop = true;
            this.radReplyString.Text = "字符串";
            this.radReplyString.UseVisualStyleBackColor = true;
            // 
            // radReplyHex
            // 
            this.radReplyHex.AutoSize = true;
            this.radReplyHex.Location = new System.Drawing.Point(264, 19);
            this.radReplyHex.Name = "radReplyHex";
            this.radReplyHex.Size = new System.Drawing.Size(118, 19);
            this.radReplyHex.TabIndex = 7;
            this.radReplyHex.Text = "十六进制文本";
            this.radReplyHex.UseVisualStyleBackColor = true;
            // 
            // txtReplyContent
            // 
            this.txtReplyContent.Location = new System.Drawing.Point(133, 68);
            this.txtReplyContent.Name = "txtReplyContent";
            this.txtReplyContent.Size = new System.Drawing.Size(411, 25);
            this.txtReplyContent.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "回复内容：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 341);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "是否忽略：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 385);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "是否自动回复：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "回复类型：";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(272, 566);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(105, 39);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnConditionAdd
            // 
            this.btnConditionAdd.Location = new System.Drawing.Point(592, 95);
            this.btnConditionAdd.Name = "btnConditionAdd";
            this.btnConditionAdd.Size = new System.Drawing.Size(39, 33);
            this.btnConditionAdd.TabIndex = 14;
            this.btnConditionAdd.Text = "+";
            this.btnConditionAdd.UseVisualStyleBackColor = true;
            this.btnConditionAdd.Click += new System.EventHandler(this.btnConditionAdd_Click);
            // 
            // btnConditionRemove
            // 
            this.btnConditionRemove.Location = new System.Drawing.Point(592, 143);
            this.btnConditionRemove.Name = "btnConditionRemove";
            this.btnConditionRemove.Size = new System.Drawing.Size(39, 33);
            this.btnConditionRemove.TabIndex = 15;
            this.btnConditionRemove.Text = "-";
            this.btnConditionRemove.UseVisualStyleBackColor = true;
            this.btnConditionRemove.Click += new System.EventHandler(this.btnConditionRemove_Click);
            // 
            // panelReply
            // 
            this.panelReply.Controls.Add(this.label3);
            this.panelReply.Controls.Add(this.radReplyString);
            this.panelReply.Controls.Add(this.radReplyHex);
            this.panelReply.Controls.Add(this.txtReplyContent);
            this.panelReply.Controls.Add(this.label6);
            this.panelReply.Enabled = false;
            this.panelReply.Location = new System.Drawing.Point(22, 431);
            this.panelReply.Name = "panelReply";
            this.panelReply.Size = new System.Drawing.Size(629, 112);
            this.panelReply.TabIndex = 16;
            // 
            // btnConditionDetail
            // 
            this.btnConditionDetail.Location = new System.Drawing.Point(592, 196);
            this.btnConditionDetail.Name = "btnConditionDetail";
            this.btnConditionDetail.Size = new System.Drawing.Size(39, 33);
            this.btnConditionDetail.TabIndex = 17;
            this.btnConditionDetail.Text = "...";
            this.btnConditionDetail.UseVisualStyleBackColor = true;
            this.btnConditionDetail.Click += new System.EventHandler(this.btnConditionDetail_Click);
            // 
            // FormIgnorAndReplyAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 617);
            this.Controls.Add(this.btnConditionDetail);
            this.Controls.Add(this.panelReply);
            this.Controls.Add(this.btnConditionRemove);
            this.Controls.Add(this.btnConditionAdd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chkAutoReply);
            this.Controls.Add(this.chkIgnor);
            this.Controls.Add(this.dataGridConditions);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.label1);
            this.MinimizeBox = false;
            this.Name = "FormIgnorAndReplyAdd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "新增忽略或自动回复项目";
            this.Load += new System.EventHandler(this.FormIgnorAndReplyAdd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridConditions)).EndInit();
            this.panelReply.ResumeLayout(false);
            this.panelReply.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridConditions;
        private System.Windows.Forms.CheckBox chkIgnor;
        private System.Windows.Forms.CheckBox chkAutoReply;
        private System.Windows.Forms.RadioButton radReplyString;
        private System.Windows.Forms.RadioButton radReplyHex;
        private System.Windows.Forms.TextBox txtReplyContent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnConditionAdd;
        private System.Windows.Forms.Button btnConditionRemove;
        private System.Windows.Forms.Panel panelReply;
        private System.Windows.Forms.Button btnConditionDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}