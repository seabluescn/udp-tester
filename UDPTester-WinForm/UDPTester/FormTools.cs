﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UDPTester
{
    public partial class FormTools : Form
    {
        public FormTools()
        {
            InitializeComponent();
        }

        #region CRC
        private void btnMakeCRC_Click(object sender, EventArgs e)
        {
            try
            {
                string hexstr = this.txtCRCinput.Text.Trim();
                hexstr += " 00 00";
                if (hexstr.Length > 1)
                {
                    var CRC = ConvertHelper.HexStringToByteArray(hexstr);
                    CRC = ConvertHelper.CalCRC16a(CRC);
                    var CRCstr = ConvertHelper.ByteArrayToHexString(CRC);
                    this.txtCRCoutput.Text = CRCstr;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 字符串转十六进制
        private void btnStr2Hex_Click(object sender, EventArgs e)
        {
            string str = this.txtStr2Hex_In.Text.Trim();

            if (!string.IsNullOrEmpty(str))
            {
                try
                {
                    var bytes = Encoding.UTF8.GetBytes(str);
                    this.txtStr2Hex_Out.Text = ConvertHelper.ByteArrayToHexString(bytes);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnHex2Str_Click(object sender, EventArgs e)
        {
            string hexstr = this.txtHex2Str_In.Text.Trim();

            if (!string.IsNullOrEmpty(hexstr))
            {
                try
                {
                    var bytes = ConvertHelper.HexStringToByteArray(hexstr);
                    this.txtHex2Str_Out.Text = Encoding.UTF8.GetString(bytes);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        #endregion


        #region 进制转换
        #region 整数
        private void txtDecimal_TextChanged(object sender, EventArgs e)
        {
            var DecStr = this.txtDecimal.Text.Trim();
            if (!string.IsNullOrEmpty(DecStr))
            {
                if (int.TryParse(DecStr, out int result))
                {
                    this.txtHexadecimal.Text = Convert.ToString(result, 16);
                    this.txtBinary.Text = Convert.ToString(result, 2);

                    this.lblStatus.Visible = false;
                }
                else
                {
                    this.lblStatus.Text = "error";
                    this.lblStatus.Visible = true;
                }
            }
        }

        private void txtHexadecimal_TextChanged(object sender, EventArgs e)
        {
            var HexStr = this.txtHexadecimal.Text.Trim();
            if (!string.IsNullOrEmpty(HexStr))
            {
                try
                {
                    int result = int.Parse(HexStr, System.Globalization.NumberStyles.HexNumber);
                    this.txtDecimal.Text = result.ToString();
                    this.txtBinary.Text = Convert.ToString(result, 2);
                }
                catch
                {
                    this.lblStatus.Text = "error";
                    this.lblStatus.Visible = true;
                }
            }
        }

        private void txtBinary_TextChanged(object sender, EventArgs e)
        {
            var BinStr = this.txtBinary.Text.Trim();
            if (!string.IsNullOrEmpty(BinStr))
            {
                try
                {
                    int result = Convert.ToInt32(BinStr, 2);
                    this.txtDecimal.Text = result.ToString();
                    this.txtHexadecimal.Text = Convert.ToString(result, 16);

                }
                catch
                {
                    this.lblStatus.Text = "error";
                    this.lblStatus.Visible = true;
                }
            }
        }

        #endregion

        #region 浮点数

        //浮点数转整数
        private void txtFloat_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string floatstr = this.txtFloat.Text.Trim();
                float floatVal = float.Parse(floatstr, System.Globalization.NumberStyles.Float);
                byte[] bytes = BitConverter.GetBytes(floatVal);
                var HexStr = BitConverter.ToInt32(bytes, 0).ToString("X8");
                this.txtFloatHex.Text = HexStr;

                int result = int.Parse(HexStr, System.Globalization.NumberStyles.HexNumber);
                this.txtFloatDecimal.Text = result.ToString();

                this.lblFloatStatus.Visible = false;
            }
            catch (Exception ex)
            {
                this.txtFloatHex.Text = "";
                this.txtFloatDecimal.Text = "";
                this.lblFloatStatus.Text = $"error:{ex.Message}";
                this.lblFloatStatus.Visible = true;
            }
        }


        //整数转浮点数
        private void txtFloatDecimal_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string decimalstr = this.txtFloatDecimal.Text.Trim();
                int decimalVal = Convert.ToInt32(decimalstr);
                byte[] bytes = BitConverter.GetBytes(decimalVal);

                var HexStrHex = BitConverter.ToInt32(bytes, 0).ToString("X8");
                this.txtFloatHex.Text = HexStrHex;

                float floatVal = BitConverter.ToSingle(bytes, 0);
                this.txtFloat.Text = floatVal.ToString();

                this.lblFloatStatus.Visible = false;
            }
            catch (Exception ex)
            {
                this.txtFloatHex.Text = "";
                this.txtFloat.Text = "";
                this.lblFloatStatus.Text = $"error:{ex.Message}";
                this.lblFloatStatus.Visible = true;
            }
        }


        #endregion

        #endregion




    }
}
