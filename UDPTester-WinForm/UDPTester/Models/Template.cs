﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPTester
{
    public class Template
    {
        public TemplateType TemplateType { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }

    public enum TemplateType
    {
        String = 0,
        Hex = 1,
    }
}
