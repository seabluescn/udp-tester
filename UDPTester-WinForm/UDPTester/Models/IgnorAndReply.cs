﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPTester.Models
{
    /// <summary>
    /// 忽略与指定回复
    /// </summary>
    [Serializable]
    public class IgnorAndReply
    {
        /// <summary>
        /// 标识
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 条件列表
        /// </summary>
        public BindingList<Condition> ConditionList { get; set; } = new BindingList<Condition>();

        /// <summary>
        /// 是否忽略显示
        /// </summary>
        public bool IsIgnor { get; set; }

        /// <summary>
        /// 是否自动回复
        /// </summary>
        public bool IsAutoReply { get; set; }

        /// <summary>
        /// 回复类型(字符串或十六进制文本)
        /// </summary>
        public TemplateType ReplyType { get; set; }

        /// <summary>
        /// 回复内容
        /// </summary>
        public string ReplyText { get; set; }
    }
}
