﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPTester.Models
{
    /// <summary>
    /// 匹配条件
    /// </summary>
    [Serializable]
    public class Condition
    {
        /// <summary>
        /// 联合类型
        /// </summary>
        public JoinType JoinType { get; set; }

        /// <summary>
        /// 条件类型
        /// </summary>
        public ConditionType ConditionType { get; set; }

        /// <summary>
        /// 条件内容
        /// </summary>
        public string Content { get; set; }       
    }
}
