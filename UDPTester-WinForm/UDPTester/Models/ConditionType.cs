﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPTester.Models
{
    public enum ConditionType
    {
        /// <summary>
        /// 完全等于
        /// </summary>
        Equal,

        /// <summary>
        /// 以XX开始
        /// </summary>
        StartWith,

        /// <summary>
        /// 以XX结束
        /// </summary>
        EndWith,

        /// <summary>
        /// 包含
        /// </summary>
        Contain, 

        /// <summary>
        /// 满足正则表达式
        /// </summary>
        Regular
    }
}
