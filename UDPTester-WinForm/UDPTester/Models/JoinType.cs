﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UDPTester.Models
{
    /// <summary>
    /// 条件联合类型
    /// </summary>
    public enum JoinType
    {
        /// <summary>
        /// 与
        /// </summary>
        And,

        /// <summary>
        /// 或
        /// </summary>
        OR
    }
}
