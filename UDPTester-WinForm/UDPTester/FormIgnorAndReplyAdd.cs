﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UDPTester.Models;

namespace UDPTester
{
    public partial class FormIgnorAndReplyAdd : Form
    {
        public IgnorAndReply IgnorAndReplyItem { get; set; }

        public FormIgnorAndReplyAdd()
        {
            InitializeComponent();
        }

        private void FormIgnorAndReplyAdd_Load(object sender, EventArgs e)
        {
            this.txtTitle.Text = IgnorAndReplyItem.Title;
            this.dataGridConditions.DataSource = IgnorAndReplyItem.ConditionList;
            this.chkIgnor.Checked = IgnorAndReplyItem.IsIgnor;
            this.chkAutoReply.Checked = IgnorAndReplyItem.IsAutoReply;

            this.radReplyString.Checked = IgnorAndReplyItem.ReplyType == TemplateType.String;
            this.radReplyHex.Checked = IgnorAndReplyItem.ReplyType == TemplateType.Hex;
            this.txtReplyContent.Text = IgnorAndReplyItem.ReplyText;
        }

        private void btnConditionAdd_Click(object sender, EventArgs e)
        {
            FormIgnorAndReplyAddCondition formAddCondition = new FormIgnorAndReplyAddCondition();
            formAddCondition.Condition = new Condition();
            if (formAddCondition.ShowDialog() == DialogResult.OK)
            {
                IgnorAndReplyItem.ConditionList.Add(formAddCondition.Condition);
            }
        }

        private void btnConditionRemove_Click(object sender, EventArgs e)
        {
            if (dataGridConditions.SelectedRows != null)
            {
                IgnorAndReplyItem.ConditionList.RemoveAt(dataGridConditions.SelectedRows[0].Index);
               
            }
            else
            {
                MessageBox.Show("请选择要删除的条件项");
            }
        }

        private void btnConditionDetail_Click(object sender, EventArgs e)
        {
            if (dataGridConditions.SelectedRows != null)
            {
                var condition = IgnorAndReplyItem.ConditionList[dataGridConditions.SelectedRows[0].Index];

                FormIgnorAndReplyAddCondition formDetail = new FormIgnorAndReplyAddCondition();
                formDetail.Condition = condition;

                if (formDetail.ShowDialog() == DialogResult.OK)
                {
                    dataGridConditions.Refresh();
                }
            }
            else
            {
                MessageBox.Show("请选择要修改的条件项");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var Title = this.txtTitle.Text.Trim();
            if (string.IsNullOrEmpty(Title))
            {
                MessageBox.Show("标识不可为空!");
                return;
            }

            if (chkAutoReply.Checked)
            {
                var ReplyText = this.txtReplyContent.Text.Trim();
                if (string.IsNullOrEmpty(ReplyText))
                {
                    MessageBox.Show("回复文本不可为空!");
                    return;
                }
            }

            if (IgnorAndReplyItem.ConditionList.Count == 0)
            {
                MessageBox.Show("条件不可为空!");
                return;
            }

            IgnorAndReplyItem.Title = Title;
            IgnorAndReplyItem.IsIgnor = this.chkIgnor.Checked;
            IgnorAndReplyItem.IsAutoReply = chkAutoReply.Checked;
            if (chkAutoReply.Checked)
            {
                IgnorAndReplyItem.ReplyType = this.radReplyString.Checked ? TemplateType.String : TemplateType.Hex;
                IgnorAndReplyItem.ReplyText = this.txtReplyContent.Text.Trim();
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void chkAutoReply_CheckedChanged(object sender, EventArgs e)
        {
            this.panelReply.Enabled = this.chkAutoReply.Checked;
        }

      
    }
}
