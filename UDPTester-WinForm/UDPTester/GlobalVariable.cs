﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UDPTester.Models;

namespace UDPTester
{
    public class GlobalVariable
    {
        /// <summary>
        /// 忽略列表数据
        /// </summary>
        public static BindingList<IgnorAndReply> IgnorAndReplyList;
    }
}
