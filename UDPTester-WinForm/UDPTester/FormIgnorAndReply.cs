﻿using CommonUtil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UDPTester.Models;

namespace UDPTester
{
    public partial class FormIgnorAndReply : Form
    {
        public FormIgnorAndReply()
        {
            InitializeComponent();
        }

        private void FormIgnorAndReply_Load(object sender, EventArgs e)
        {
            this.dataGridList.DataSource = GlobalVariable.IgnorAndReplyList;
        }

        private void toolAdd_Click(object sender, EventArgs e)
        {
            IgnorAndReply item = new IgnorAndReply();
            FormIgnorAndReplyAdd formIgnor = new FormIgnorAndReplyAdd
            {
                IgnorAndReplyItem = item
            };
            if (formIgnor.ShowDialog() == DialogResult.OK)
            {
                GlobalVariable.IgnorAndReplyList.Add(item);
                SaveList();
            }
        }

        private void toolRemove_Click(object sender, EventArgs e)
        {
            if (this.dataGridList.SelectedRows != null)
            {
                GlobalVariable.IgnorAndReplyList.RemoveAt(this.dataGridList.SelectedRows[0].Index);
                SaveList();
            }
            else
            {
                MessageBox.Show("请选择记录");
            }
        }

        private void toolEdit_Click(object sender, EventArgs e)
        {
            if (this.dataGridList.SelectedRows != null)
            {
                int index = this.dataGridList.SelectedRows[0].Index;
                var item = GlobalVariable.IgnorAndReplyList[index];
                FormIgnorAndReplyAdd formDetail = new FormIgnorAndReplyAdd
                {
                    IgnorAndReplyItem = item
                };

                if (formDetail.ShowDialog() == DialogResult.OK)
                {
                    this.dataGridList.Refresh();
                    SaveList();
                }
            }
            else
            {
                MessageBox.Show("请选择记录");
            }
        }


        public void SaveList()
        {
            var XmlFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "IgnorAndReply.xml");
            XmlHelper.XmlSerializeToFile(GlobalVariable.IgnorAndReplyList, XmlFilePath, Encoding.UTF8);
        }

    }
}
