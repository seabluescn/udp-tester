﻿namespace UDPTester
{
    partial class FormTools
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtHex2Str_In = new System.Windows.Forms.TextBox();
            this.btnHex2Str = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHex2Str_Out = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtStr2Hex_In = new System.Windows.Forms.TextBox();
            this.btnStr2Hex = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStr2Hex_Out = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblStatus = new System.Windows.Forms.Label();
            this.txtBinary = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtHexadecimal = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDecimal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.btnMakeCRC = new System.Windows.Forms.Button();
            this.txtCRCoutput = new System.Windows.Forms.TextBox();
            this.txtCRCinput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtFloatDecimal = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFloat = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFloatHex = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lblFloatStatus = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(857, 579);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(849, 550);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "字符串互转";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtHex2Str_In);
            this.groupBox2.Controls.Add(this.btnHex2Str);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtHex2Str_Out);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(17, 271);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(809, 234);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "16进制文本转字符串";
            // 
            // txtHex2Str_In
            // 
            this.txtHex2Str_In.Location = new System.Drawing.Point(101, 52);
            this.txtHex2Str_In.Name = "txtHex2Str_In";
            this.txtHex2Str_In.Size = new System.Drawing.Size(659, 25);
            this.txtHex2Str_In.TabIndex = 7;
            // 
            // btnHex2Str
            // 
            this.btnHex2Str.Location = new System.Drawing.Point(101, 160);
            this.btnHex2Str.Name = "btnHex2Str";
            this.btnHex2Str.Size = new System.Drawing.Size(114, 39);
            this.btnHex2Str.TabIndex = 9;
            this.btnHex2Str.Text = "转换";
            this.btnHex2Str.UseVisualStyleBackColor = true;
            this.btnHex2Str.Click += new System.EventHandler(this.btnHex2Str_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "输入：";
            // 
            // txtHex2Str_Out
            // 
            this.txtHex2Str_Out.Location = new System.Drawing.Point(101, 108);
            this.txtHex2Str_Out.Name = "txtHex2Str_Out";
            this.txtHex2Str_Out.ReadOnly = true;
            this.txtHex2Str_Out.Size = new System.Drawing.Size(659, 25);
            this.txtHex2Str_Out.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "输出";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtStr2Hex_In);
            this.groupBox1.Controls.Add(this.btnStr2Hex);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtStr2Hex_Out);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(17, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(809, 234);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "字符串转16进制文本";
            // 
            // txtStr2Hex_In
            // 
            this.txtStr2Hex_In.Location = new System.Drawing.Point(101, 52);
            this.txtStr2Hex_In.Name = "txtStr2Hex_In";
            this.txtStr2Hex_In.Size = new System.Drawing.Size(659, 25);
            this.txtStr2Hex_In.TabIndex = 7;
            // 
            // btnStr2Hex
            // 
            this.btnStr2Hex.Location = new System.Drawing.Point(101, 160);
            this.btnStr2Hex.Name = "btnStr2Hex";
            this.btnStr2Hex.Size = new System.Drawing.Size(114, 39);
            this.btnStr2Hex.TabIndex = 9;
            this.btnStr2Hex.Text = "转换";
            this.btnStr2Hex.UseVisualStyleBackColor = true;
            this.btnStr2Hex.Click += new System.EventHandler(this.btnStr2Hex_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "输入：";
            // 
            // txtStr2Hex_Out
            // 
            this.txtStr2Hex_Out.Location = new System.Drawing.Point(101, 108);
            this.txtStr2Hex_Out.Name = "txtStr2Hex_Out";
            this.txtStr2Hex_Out.ReadOnly = true;
            this.txtStr2Hex_Out.Size = new System.Drawing.Size(659, 25);
            this.txtStr2Hex_Out.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "输出";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(849, 550);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "进制转换";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(258, 186);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(63, 15);
            this.lblStatus.TabIndex = 6;
            this.lblStatus.Text = "Success";
            this.lblStatus.Visible = false;
            // 
            // txtBinary
            // 
            this.txtBinary.Location = new System.Drawing.Point(258, 145);
            this.txtBinary.Name = "txtBinary";
            this.txtBinary.Size = new System.Drawing.Size(334, 25);
            this.txtBinary.TabIndex = 5;
            this.txtBinary.TextChanged += new System.EventHandler(this.txtBinary_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(154, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 4;
            this.label9.Text = "二进制：";
            // 
            // txtHexadecimal
            // 
            this.txtHexadecimal.Location = new System.Drawing.Point(258, 98);
            this.txtHexadecimal.Name = "txtHexadecimal";
            this.txtHexadecimal.Size = new System.Drawing.Size(334, 25);
            this.txtHexadecimal.TabIndex = 3;
            this.txtHexadecimal.TextChanged += new System.EventHandler(this.txtHexadecimal_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(154, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "十六进制：";
            // 
            // txtDecimal
            // 
            this.txtDecimal.Location = new System.Drawing.Point(258, 49);
            this.txtDecimal.Name = "txtDecimal";
            this.txtDecimal.Size = new System.Drawing.Size(334, 25);
            this.txtDecimal.TabIndex = 1;
            this.txtDecimal.TextChanged += new System.EventHandler(this.txtDecimal_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(154, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "十进制：";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.btnMakeCRC);
            this.tabPage3.Controls.Add(this.txtCRCoutput);
            this.tabPage3.Controls.Add(this.txtCRCinput);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(849, 550);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "CRC校验";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(104, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(298, 15);
            this.label10.TabIndex = 5;
            this.label10.Text = "校验标准：CRC-16/MODBUS(x16+x15+x2+1)";
            // 
            // btnMakeCRC
            // 
            this.btnMakeCRC.Location = new System.Drawing.Point(107, 245);
            this.btnMakeCRC.Name = "btnMakeCRC";
            this.btnMakeCRC.Size = new System.Drawing.Size(114, 39);
            this.btnMakeCRC.TabIndex = 4;
            this.btnMakeCRC.Text = "生成";
            this.btnMakeCRC.UseVisualStyleBackColor = true;
            this.btnMakeCRC.Click += new System.EventHandler(this.btnMakeCRC_Click);
            // 
            // txtCRCoutput
            // 
            this.txtCRCoutput.Location = new System.Drawing.Point(107, 177);
            this.txtCRCoutput.Name = "txtCRCoutput";
            this.txtCRCoutput.ReadOnly = true;
            this.txtCRCoutput.Size = new System.Drawing.Size(696, 25);
            this.txtCRCoutput.TabIndex = 3;
            // 
            // txtCRCinput
            // 
            this.txtCRCinput.Location = new System.Drawing.Point(107, 112);
            this.txtCRCinput.Name = "txtCRCinput";
            this.txtCRCinput.Size = new System.Drawing.Size(696, 25);
            this.txtCRCinput.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "输出";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "输入：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtBinary);
            this.groupBox3.Controls.Add(this.lblStatus);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtDecimal);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtHexadecimal);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(837, 230);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "32位整数";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblFloatStatus);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.txtFloatDecimal);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.txtFloat);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtFloatHex);
            this.groupBox4.Location = new System.Drawing.Point(6, 242);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(837, 302);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "32位单精度浮点数";
            // 
            // txtFloatDecimal
            // 
            this.txtFloatDecimal.Location = new System.Drawing.Point(259, 206);
            this.txtFloatDecimal.Name = "txtFloatDecimal";
            this.txtFloatDecimal.Size = new System.Drawing.Size(334, 25);
            this.txtFloatDecimal.TabIndex = 12;
            this.txtFloatDecimal.TextChanged += new System.EventHandler(this.txtFloatDecimal_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(155, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 15);
            this.label12.TabIndex = 7;
            this.label12.Text = "浮点数：";
            // 
            // txtFloat
            // 
            this.txtFloat.Location = new System.Drawing.Point(259, 110);
            this.txtFloat.Name = "txtFloat";
            this.txtFloat.Size = new System.Drawing.Size(334, 25);
            this.txtFloat.TabIndex = 8;
            this.txtFloat.TextChanged += new System.EventHandler(this.txtFloat_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(155, 206);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 15);
            this.label13.TabIndex = 11;
            this.label13.Text = "十进制：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(155, 159);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 15);
            this.label14.TabIndex = 9;
            this.label14.Text = "十六进制：";
            // 
            // txtFloatHex
            // 
            this.txtFloatHex.Location = new System.Drawing.Point(259, 159);
            this.txtFloatHex.Name = "txtFloatHex";
            this.txtFloatHex.ReadOnly = true;
            this.txtFloatHex.Size = new System.Drawing.Size(334, 25);
            this.txtFloatHex.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(187, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(405, 15);
            this.label15.TabIndex = 14;
            this.label15.Text = "IEEE 754浮点数(32位,四字节,单精度)与32位整数相互转换";
            // 
            // lblFloatStatus
            // 
            this.lblFloatStatus.AutoSize = true;
            this.lblFloatStatus.Location = new System.Drawing.Point(256, 255);
            this.lblFloatStatus.Name = "lblFloatStatus";
            this.lblFloatStatus.Size = new System.Drawing.Size(63, 15);
            this.lblFloatStatus.TabIndex = 15;
            this.lblFloatStatus.Text = "Success";
            this.lblFloatStatus.Visible = false;
            // 
            // FormTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 603);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormTools";
            this.Text = "Tools";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnMakeCRC;
        private System.Windows.Forms.TextBox txtCRCoutput;
        private System.Windows.Forms.TextBox txtCRCinput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtHex2Str_In;
        private System.Windows.Forms.Button btnHex2Str;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtHex2Str_Out;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtStr2Hex_In;
        private System.Windows.Forms.Button btnStr2Hex;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStr2Hex_Out;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBinary;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtHexadecimal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDecimal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtFloatDecimal;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtFloat;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFloatHex;
        private System.Windows.Forms.Label lblFloatStatus;
    }
}