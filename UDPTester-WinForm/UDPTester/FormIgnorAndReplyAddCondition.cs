﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UDPTester.Models;

namespace UDPTester
{
    public partial class FormIgnorAndReplyAddCondition : Form
    {
        public Condition Condition { get; set; }

        public FormIgnorAndReplyAddCondition()
        {
            InitializeComponent();
        }

        private void FormIgnorAndReplyAddCondition_Load(object sender, EventArgs e)
        {
            radJoinAnd.Checked = Condition.JoinType == JoinType.And;
            radJoinOr.Checked = Condition.JoinType == JoinType.OR;

            radEqual.Checked = Condition.ConditionType == ConditionType.Equal;
            radStartWith.Checked = Condition.ConditionType == ConditionType.StartWith;
            radEndWith.Checked = Condition.ConditionType == ConditionType.EndWith;
            radContain.Checked = Condition.ConditionType == ConditionType.Contain;
            radRegular.Checked = Condition.ConditionType == ConditionType.Regular;

            txtConditionText.Text = Condition.Content;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string ConditionText = this.txtConditionText.Text.Trim();

            if (string.IsNullOrEmpty(ConditionText))
            {
                MessageBox.Show("必须输入条件文本!");
                return;
            }

            ConditionType conditionType = ConditionType.Equal;
            if (radEqual.Checked) conditionType = ConditionType.Equal;
            if (radStartWith.Checked) conditionType = ConditionType.StartWith;
            if (radEndWith.Checked) conditionType = ConditionType.EndWith;
            if (radContain.Checked) conditionType = ConditionType.Contain;
            if (radRegular.Checked) conditionType = ConditionType.Regular;

            Condition.JoinType = radJoinAnd.Checked ? JoinType.And : JoinType.OR;
            Condition.ConditionType = conditionType;
            Condition.Content = ConditionText;          

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
