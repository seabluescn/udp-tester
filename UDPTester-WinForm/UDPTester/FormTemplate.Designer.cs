﻿namespace UDPTester
{
    partial class FormTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTemplate));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.nenuFilterAll = new System.Windows.Forms.ToolStripMenuItem();
            this.nenuFilterString = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFilterHex = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuAdd = new System.Windows.Forms.ToolStripButton();
            this.menuRemove = new System.Windows.Forms.ToolStripButton();
            this.dataGridTemplates = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSelect = new System.Windows.Forms.Button();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTemplates)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(30, 30);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripSeparator1,
            this.menuAdd,
            this.menuRemove});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(778, 37);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nenuFilterAll,
            this.nenuFilterString,
            this.menuFilterHex});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(83, 34);
            this.toolStripDropDownButton1.Text = "筛选";
            // 
            // nenuFilterAll
            // 
            this.nenuFilterAll.Name = "nenuFilterAll";
            this.nenuFilterAll.Size = new System.Drawing.Size(182, 26);
            this.nenuFilterAll.Text = "全部";
            // 
            // nenuFilterString
            // 
            this.nenuFilterString.Name = "nenuFilterString";
            this.nenuFilterString.Size = new System.Drawing.Size(182, 26);
            this.nenuFilterString.Text = "字符串";
            // 
            // menuFilterHex
            // 
            this.menuFilterHex.Name = "menuFilterHex";
            this.menuFilterHex.Size = new System.Drawing.Size(182, 26);
            this.menuFilterHex.Text = "十六进制文本";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 37);
            // 
            // menuAdd
            // 
            this.menuAdd.Image = ((System.Drawing.Image)(resources.GetObject("menuAdd.Image")));
            this.menuAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuAdd.Name = "menuAdd";
            this.menuAdd.Size = new System.Drawing.Size(73, 34);
            this.menuAdd.Text = "添加";
            this.menuAdd.Click += new System.EventHandler(this.menuAdd_Click);
            // 
            // menuRemove
            // 
            this.menuRemove.Image = ((System.Drawing.Image)(resources.GetObject("menuRemove.Image")));
            this.menuRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuRemove.Name = "menuRemove";
            this.menuRemove.Size = new System.Drawing.Size(73, 34);
            this.menuRemove.Text = "移除";
            this.menuRemove.Click += new System.EventHandler(this.menuRemove_Click);
            // 
            // dataGridTemplates
            // 
            this.dataGridTemplates.AllowUserToAddRows = false;
            this.dataGridTemplates.AllowUserToDeleteRows = false;
            this.dataGridTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridTemplates.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridTemplates.Location = new System.Drawing.Point(0, 40);
            this.dataGridTemplates.MultiSelect = false;
            this.dataGridTemplates.Name = "dataGridTemplates";
            this.dataGridTemplates.ReadOnly = true;
            this.dataGridTemplates.RowHeadersWidth = 51;
            this.dataGridTemplates.RowTemplate.Height = 27;
            this.dataGridTemplates.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridTemplates.Size = new System.Drawing.Size(778, 492);
            this.dataGridTemplates.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "TemplateType";
            this.Column1.HeaderText = "Type";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 125;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Title";
            this.Column2.HeaderText = "Title";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Width = 125;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Content";
            this.Column3.HeaderText = "Content";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Width = 325;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(12, 541);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(118, 41);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "选择";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // FormTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 589);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.dataGridTemplates);
            this.Controls.Add(this.toolStrip1);
            this.MaximizeBox = false;
            this.Name = "FormTemplate";
            this.Text = "Template";
            this.Load += new System.EventHandler(this.FormTemplate_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTemplates)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton menuAdd;
        private System.Windows.Forms.ToolStripButton menuRemove;
        private System.Windows.Forms.DataGridView dataGridTemplates;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem nenuFilterAll;
        private System.Windows.Forms.ToolStripMenuItem nenuFilterString;
        private System.Windows.Forms.ToolStripMenuItem menuFilterHex;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}