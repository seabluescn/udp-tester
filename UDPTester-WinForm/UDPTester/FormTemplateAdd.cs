﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UDPTester
{
    public partial class FormTemplateAdd : Form
    {
        public Template Template { get; set; }

        public FormTemplateAdd()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var Title= this.txtTitle.Text.Trim();
            var Content = this.txtContent.Text.Trim();

            if(Title.Length==0||Content.Length==0)
            {
                MessageBox.Show("请输入标识和内容");
                return;
            }

            Template = new Template
            {
                TemplateType = radString.Checked ? TemplateType.String : TemplateType.Hex,
                Title = Title,
                Content = Content
            };

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
