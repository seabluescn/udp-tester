# UdpTester

#### 介绍
两个UDP通信测试的工具，一个WinForm版本，一个WPF版本。
两个版本功能基本一致。

![WinForm](https://seabluescn.gitee.io/webs/images/udp_win.png "udp_win.png")

![WPF](https://seabluescn.gitee.io/webs/images/udp_wpf.png "udp_wpf.png")


